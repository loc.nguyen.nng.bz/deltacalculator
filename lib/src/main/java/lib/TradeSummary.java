package lib;

import velox.api.layer1.annotations.Layer1ApiVersion;
import velox.api.layer1.annotations.Layer1ApiVersionValue;
import velox.api.layer1.annotations.Layer1SimpleAttachable;
import velox.api.layer1.annotations.Layer1StrategyName;
import velox.api.layer1.common.Log;
import velox.api.layer1.data.InstrumentInfo;
import velox.api.layer1.data.TradeInfo;
import velox.api.layer1.simplified.*;

import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.TreeMap;

@Layer1SimpleAttachable
@Layer1StrategyName("Trade summary")
@Layer1ApiVersion(Layer1ApiVersionValue.VERSION2)
public class TradeSummary implements CustomModule, TradeDataListener, HistoricalDataListener, TimeListener, IntervalListener, BackfilledDataListener {
    public static long startTime = 0;
    public static long endTime = 0;
    private final TreeMap<Double, TreeMap<Long, Integer>> bids = new TreeMap<>(Comparator.reverseOrder());       // price: {time: size}
    private final TreeMap<Double, TreeMap<Long, Integer>> asks = new TreeMap<>(Comparator.reverseOrder());       // 125: {13h00: 100, 13:01: 200, 13h02: 50}
    private PainterFactory painterFactory;
    private Api api;
    private long timestamp = 0;
    private double pips;

    @Override
    public void initialize(String alias, InstrumentInfo info, Api api, InitialState initialState) {
        this.pips = info.pips;
        this.api = api;
        this.painterFactory = new PainterFactory(alias);
        api.sendUserMessage(this.painterFactory.getUserMessage(true));
    }
    
    @Override
    public void stop() {
        api.sendUserMessage(this.painterFactory.getUserMessage(false));
    }

    @Override
    public void onTrade(double price, int size, TradeInfo tradeInfo) {
        TreeMap<Double, TreeMap<Long, Integer>> trades = tradeInfo.isBidAggressor ? this.bids : this.asks;

        if (size > 0) {
            DecimalFormat priceFormat = new DecimalFormat("#.##");
            price = Double.parseDouble(priceFormat.format(price * this.pips));
            if (!trades.containsKey(price)) {
                trades.put(price, new TreeMap<>());
            }

            TreeMap<Long, Integer> records = trades.get(price);
            Integer accumulatedSize = 0;
            if (records.containsKey(this.timestamp)) {
                accumulatedSize = records.get(this.timestamp);
            }
            records.put(this.timestamp, accumulatedSize + size);
        }
    }

    @Override
    public long getInterval() {
        return Intervals.INTERVAL_200_MILLISECONDS;
    }

    @Override
    public void onInterval() {
        int totalBids = this.getTotalSize(true);
        int totalAsks = this.getTotalSize(false);
        int delta = (totalBids - totalAsks);

        this.painterFactory.update(totalBids, totalAsks, delta);
    }

    @Override
    public void onTimestamp(long time) {
        this.timestamp = time;
    }

    private int getTotalSize(boolean isBidAggressor) {
        if (isBidAggressor) {
            Log.info("---------------------------------------------- BIDS --------------------------------------------------------------");
        }
        else {
            Log.info("---------------------------------------------- ASKS --------------------------------------------------------------");
        }

        TreeMap<Double, TreeMap<Long, Integer>> trades = isBidAggressor? this.bids : this.asks;
        int totalSize = 0;

        for (Double price : trades.keySet()) {
            int size = 0;
            TreeMap<Long, Integer> records = trades.get(price);
            for (Long time : records.keySet()) {
                if (startTime > 0 && endTime > 0 && (time < startTime || time > endTime)) {
                    continue;
                }
                size += records.get(time);
            }
            if (size == 0)
                continue;

            Log.info(String.format("-------- price: %s    size: %s", price, size));
            totalSize += size;
        }
        Log.info(String.format("----------------------- total: %s", totalSize));
        return totalSize;
    }

}
