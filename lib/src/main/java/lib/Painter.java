package lib;

import velox.api.layer1.layers.strategies.interfaces.ScreenSpaceCanvas;
import velox.api.layer1.layers.strategies.interfaces.ScreenSpaceCanvas.*;
import velox.api.layer1.layers.strategies.interfaces.ScreenSpaceCanvasFactory;
import velox.api.layer1.layers.strategies.interfaces.ScreenSpacePainterAdapter;

import java.awt.*;
import java.awt.image.BufferedImage;

class Painter implements ScreenSpacePainterAdapter {
    private final ScreenSpaceCanvas canvas;
    private final String alias;
    private CanvasIcon plankIcon;
    private int heatmapPixelsHeight;
    private int totalBids = 0;
    private int totalAsks = 0;
    private int delta = 0;
    private long startTime = 0;
    private long activeTimeWidth = 0;
    public Painter(ScreenSpaceCanvasFactory screenSpaceCanvasFactory, String alias) {
        super();
        this.canvas = screenSpaceCanvasFactory.createCanvas(ScreenSpaceCanvasFactory.ScreenSpaceCanvasType.HEATMAP);
        this.alias = alias;
    }
    public void onTradeUpdated(int totalBid, int totalAsk, int delta) {
        this.totalBids = totalBid;
        this.totalAsks = totalAsk;
        this.delta = delta;

        if (heatmapPixelsHeight > 0) {
            update();
        }
    }
    @Override
    public void onHeatmapPixelsHeight(int heatmapPixelsHeight) {
        this.heatmapPixelsHeight = heatmapPixelsHeight;
    }
    @Override
    public void onHeatmapTimeLeft(long heatmapTimeLeft) {
        this.startTime = heatmapTimeLeft;
    }
    @Override
    public void onHeatmapActiveTimeWidth(long heatmapActiveTimeWidth) {
        this.activeTimeWidth = heatmapActiveTimeWidth;
    }
    @Override
    public void onMoveEnd() {
        update();
        TradeSummary.startTime = this.startTime;
        TradeSummary.endTime = this.startTime + this.activeTimeWidth;
    }

    private synchronized void update() {
        CompositeHorizontalCoordinate x1 = new ScreenSpaceCanvas.CompositeHorizontalCoordinate(ScreenSpaceCanvas.CompositeCoordinateBase.PIXEL_ZERO, 0, 0);
        CompositeVerticalCoordinate y1 = new ScreenSpaceCanvas.CompositeVerticalCoordinate(ScreenSpaceCanvas.CompositeCoordinateBase.PIXEL_ZERO, heatmapPixelsHeight - 100, 0);
        CompositeHorizontalCoordinate x2 = new ScreenSpaceCanvas.CompositeHorizontalCoordinate(ScreenSpaceCanvas.CompositeCoordinateBase.PIXEL_ZERO, 300, 0);
        CompositeVerticalCoordinate y2 = new ScreenSpaceCanvas.CompositeVerticalCoordinate(ScreenSpaceCanvas.CompositeCoordinateBase.PIXEL_ZERO, heatmapPixelsHeight, 0);

        ScreenSpaceCanvas.PreparedImage icon = generateTradeSummaryIcon(300, 100, alias, totalBids, totalAsks, delta);
        if (plankIcon == null) {
            plankIcon = new ScreenSpaceCanvas.CanvasIcon(icon, x1, y1, x2, y2);
            canvas.addShape(plankIcon);
        } else {
            plankIcon.setImage(icon);
            plankIcon.setX2(x2);
        }

        plankIcon.setY1(y1);
        plankIcon.setY2(y2);
    }

    private PreparedImage generateTradeSummaryIcon(int width, int height, String alias, int totalBid, int totalAsk, int delta) {
        BufferedImage icon = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics = (Graphics2D) icon.getGraphics();
        graphics.setBackground(Color.GRAY);
        graphics.clearRect(0, 0, icon.getWidth(), icon.getHeight());

        graphics.setColor(Color.WHITE);
        graphics.setFont(graphics.getFont().deriveFont(22f));

        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        graphics.drawString(String.format("Total bids: %s", totalBid), 0, 18);
        graphics.drawString(String.format("Total asks: %s", totalAsk), 0, 41);
        graphics.drawString(String.format("Delta: %s", delta), 0, 64);

        graphics.dispose();
        return new ScreenSpaceCanvas.PreparedImage(icon);
    }

    @Override
    public void dispose() {
        canvas.dispose();
    }
}
