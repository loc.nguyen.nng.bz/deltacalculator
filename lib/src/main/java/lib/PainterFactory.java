package lib;

import velox.api.layer1.layers.strategies.interfaces.*;
import velox.api.layer1.messages.indicators.AliasFilter;
import velox.api.layer1.messages.indicators.Layer1ApiUserMessageModifyScreenSpacePainter;

class PainterFactory implements ScreenSpacePainterFactory {
    public final String alias;
    private Painter painter;
    public PainterFactory(String alias) {
        super();
        this.alias = alias;
    }
    public void update(int totalBids, int totalAsks, int delta) {
        if (this.painter != null)
            this.painter.onTradeUpdated(totalBids, totalAsks, delta);
    }
    @Override
    public ScreenSpacePainter createScreenSpacePainter(String indicatorName, String indicatorAlias,
                                                       ScreenSpaceCanvasFactory screenSpaceCanvasFactory) {
        painter = new Painter(screenSpaceCanvasFactory, indicatorAlias);
        return painter;
    }
    public Layer1ApiUserMessageModifyScreenSpacePainter getUserMessage(boolean isAdd) {
        return Layer1ApiUserMessageModifyScreenSpacePainter
                .builder(PainterFactory.class, this.alias)
                .setIsAdd(isAdd)
                .setAliasFilter(new AliasFilter() {
                    @Override
                    public boolean isDisplayedForAlias(String alias) {
                        return PainterFactory.this.alias.equals(alias);
                    }
                })
                .setScreenSpacePainterFactory(this)
                .build();
    }
}