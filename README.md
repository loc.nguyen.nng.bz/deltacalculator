## Run this command to build bundle
    gradle jar

## Add this add-on to book-map
    On book-map, go to settings -> configure add-ons -> Add... 
    Select the lib.jar file that is generated in "path-to-project/lib/build/libs/lib.jar"
    Select "Delta calculator"